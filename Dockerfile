FROM alpine:3.12
#rethinkdb no available in 3.13
USER root

RUN apk update && \
    apk add --update bash rethinkdb supervisor py3-pip python3 && \
    rm -rf /var/cache/apk/*

VOLUME [ "/logs" ]

WORKDIR /scripts

COPY ./requirements.txt .
COPY ./prep.sh .

RUN ./prep.sh

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY . .

RUN ./setup.sh

WORKDIR /data

EXPOSE 28015

ENTRYPOINT [ "/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
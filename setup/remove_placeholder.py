import rethinkdb as r
from os import environ

def main():
    r.connect(environ.get("RETHINK_HOST", "localhost")).repl()
    r.db("Plugins").table_drop("Placeholder").run()

if __name__ == '__main__':
    main()
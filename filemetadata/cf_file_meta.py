import os
import sys
from time import time, sleep
import rethinkdb as r

_path = os.path.abspath(os.path.join(os.path.abspath(__file__), "../../"))+"/schema"
sys.path.append(_path)

from brain.static import RBF, RBM  # BRAIN_DB, FILEMETADATA,
from brain.binary import PRIMARY_FIELD, PART_FIELD
from brain.connection import connect


NEW_VAL = "new_val"
OLD_VAL = "old_val"


def establish_files_change_feed():
    conn = connect()
    files_changes = RBF.pluck((PRIMARY_FIELD, PART_FIELD)).changes(squash=False).run(conn)
    return files_changes


def loop_file_changes(wait=33, restart_change_feed=200):
    restart_time = time() + restart_change_feed
    files_cf = establish_files_change_feed()
    while True:
        try:
            files_c = files_cf.next(wait=wait)
            yield files_c
        except r.errors.ReqlTimeoutError:
            if time() > restart_time:
                files_cf = establish_files_change_feed()
                restart_time = time() + restart_change_feed


def handle_change(change):
    conn = connect()
    # put file
    if change[NEW_VAL] is not None:
        RBM.insert({
            PRIMARY_FIELD: change[NEW_VAL][PRIMARY_FIELD],
            PART_FIELD: change[NEW_VAL][PART_FIELD]
        }).run(conn)

    # delete file
    if change[NEW_VAL] is None:
        RBM.get(change[OLD_VAL][PRIMARY_FIELD]).delete().run(conn)


def start():
    # iterate forever
    for change in loop_file_changes():
        handle_change(change)


if __name__ == "__main__":
    start()

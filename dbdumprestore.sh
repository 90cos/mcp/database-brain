#!/bin/bash
if [[ $1 = 'backup' ]]; then
  today=`date '+%Y_%m_%d__%H_%M_%s'`
  filename=$today.tar.gz
  fullfilename='/logs/'$filename
  rethinkdb dump -f $fullfilename 1>&2 2> /dev/null
  if [[ $? != 0 ]]; then
    python /scripts/placeholder.py
    rethinkdb dump -f $fullfilename 1>&2 > /dev/null
  fi
  echo ""
  echo $filename
else
  if [[ $1 = 'restore' ]]; then
    filename=$2
    rethinkdb restore --force /logs/$filename
    echo ""
    echo $filename
  fi
fi
exit


import docker
import rethinkdb as r
from pytest import fixture
import time as T
from os import environ

CLIENT = docker.from_env()


@fixture(scope="module")
def something():
    container_name = "BrainAT"
    try:
        tag = environ.get("TRAVIS_BRANCH", "dev").replace("master", "latest")
    except KeyError:
        tag = "latest"
    container = CLIENT.containers.run(
        "".join(("registry.gitlab.com/90cos/mcp/database-brain:", tag)),
        name=container_name,
        detach=True,
        ports={"28015/tcp": 28015},
        remove=True
    )
    T.sleep(20)
    yield environ.get("RETHINK_HOST", "localhost")
    container.stop()


def test_run_cursor(something):
    conn = r.connect(something)
    conn2 = r.connect(something)
    cursor = r.db("Brain").table("Targets").changes().run(conn)
    r.db("Brain").table("Targets").insert(
        {"Targets": "Job_Target", "Status": "pending", "Start_Time": int(T.time()), "Job_Command": "Keylogger"}).run(conn2)
    for document in cursor:
        r.db("Audit").table("Targets").insert(document).run(conn2)
        break
    assert document

def test_audit_contains(something):
    conn2 = r.connect(something)
    cursor = r.db("Audit").table("Targets").run(conn2)
    document = None
    for document in cursor:
        print(document)
    assert document

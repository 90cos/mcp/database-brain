#!/usr/bin/python

# Using Plugin

import rethinkdb as r

def connect():
    """
    Connect to the brain
    :return:
    """
    return r.connect("localhost").repl()

def placeholdercreate():
    """
    Plugins.Placeholder table creation
    :return:
    """
    return r.db("Plugins").table_create("Placeholder").run()


if __name__ == "__main__":	 # pragma: no cover

    connect()
    try:
        placeholdercreate()
    except:
        pass

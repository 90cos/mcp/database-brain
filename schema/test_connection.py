"""
Pytest file for the connection wrapper
"""

from os import environ
from pytest import fixture, raises
import docker

from .brain import connect, r
from .brain.connection import DefaultConnection, BrainNotReady
CLIENT = docker.from_env()


@fixture(scope='module')
def rethink():
    try:
        tag = environ.get("TRAVIS_BRANCH", "dev").replace("master", "latest")
    except KeyError:
        tag = "latest"
    container_name = "brainmoduletest"
    CLIENT.containers.run(
        "registry.gitlab.com/90cos/mcp/database-brain:{}".format(tag),
        name=container_name,
        detach=True,
        ports={"28015/tcp": 28015},
        remove=True
    )
    yield True
    # Teardown for module tests
    containers = CLIENT.containers.list()
    for container in containers:
        if container.name == container_name:
            container.stop()
            break


def test_brain_interface(rethink):
    return connect()# attempt to connect to


def test_brain_interface_with_host(rethink):
    import rethinkdb
    return connect(environ.get("RETHINK_HOST", "localhost"), rethinkdb.DEFAULT_PORT)


def test_connect_rethink_host(rethink):
    rth = environ.get("RETHINK_HOST")
    environ["RETHINK_HOST"] = "test_var_pls_ignore"
    with raises(BrainNotReady):
        connect() # should attempt to connect to "test_var_pls_ignore", which doesnt exist
    if rth is not None:
        environ["RETHINK_HOST"] = str(rth)
    else:
        del environ["RETHINK_HOST"]


def test_direct_interface(rethink):
    r.connect(environ.get("RETHINK_HOST", "localhost"))


def test_rethink_host_enviornment_override(rethink):
    old_env = environ.get("RETHINK_HOST")
    new_env = "9.9.9.9"  # hope there is not a rethink host here
    environ["RETHINK_HOST"] = new_env
    try:
        connect()
        if old_env:
            environ["RETHINK_HOST"] = old_env
        else:
            del(environ["RETHINK_HOST"])
        raise ValueError("There should not be a rethink host on {}".format(new_env))
    except BrainNotReady as err:
        if old_env:
            environ["RETHINK_HOST"] = old_env
        else:
            del(environ["RETHINK_HOST"])

        assert new_env in str(err)


if __name__ == "__main__":
    a = rethink()
    c = test_brain_interface(a.__next__())
    try:
        a.__next__()
    except StopIteration:
        pass

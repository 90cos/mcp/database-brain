"""
Query module is getting big
"""

# import here for backward compatible
from ..static import RBT, RBJ, RBO, RBF, RPX, RPC, RPP, DONE, RBM
from ..static import FILEMETADATA, BRAIN_DB

CUSTOM_FILTER_NAME = "custom_filter"
IDX_OUTPUT_JOB_ID = "Output_job_id"
IDX_STATUS = "Status"

# backward compatible api
from .reads import *
from .writes import *

"""
the ..queries module of this library aimed
to exclusively perform atomic database actions

this module provides optional assist features

without the guarantee of atomic reqsponse from the database

This file performs non-atomic actions on jobs objects

"""

from ..queries.reads import get_next_job as orig_get_next_job
from ..queries.writes import update_job_status
from ..queries.writes import write_output as orig_write_output
from .static import STATUS
from ..static import ID_FIELD, DONE, ERROR, STATUS_FIELD


def get_next_job(plugin_name, location=None, port=None,
                 verify_job=True, conn=None, **kwargs):
    """
    potentially non-atomic database action

    accepts kwarg status=<STATUS>
    <STATUS> is defined in brain.jobs.STATES dictionary

    there is a moment between this library collecting the job
            and this library setting the new state.

    :raises brain.jobs.InvalidStateTransition if status kwarg is an illegal Transition
    :raises brain.jobs.InvalidState id status kwarg is not a valid state
    :raises ValueError if anything went wrong with rehtinkdb


    :param plugin_name: <str>
    :param location:    <str>
    :param port:        <str>
    :param verify_job:  <bool>
    :param conn:        <rethink.DefaultConnection>
    :param kwargs:      <Keyword Arguments>
    :return:
    """

    job = orig_get_next_job(plugin_name, location, port, verify_job, conn=conn, **kwargs)
    if job and STATUS in kwargs:
        job[STATUS_FIELD] = kwargs[STATUS]  # local update
        update_job_status(job[ID_FIELD], kwargs[STATUS])  # remote update
    return job


def write_output(job_id, content, conn=None):
    """
    potentially non-atomic database action

    this assumes the caller is performing the standard job transition
        Waiting>Ready>Pending>Done

    this will work if the caller performs the augmented job transition
        Waiting>Ready>Pending>Active>Done

    """
    result = orig_write_output(job_id, content, conn=conn)
    if result:
        update_job_status(job_id, DONE, conn=conn)
    return result


def write_error(job_id, content, conn=None):
    """
    potentially non-atomic database action

    this assumes the caller is performing the standard job transition
        Waiting>Ready>Pending>Error

    this will work if the caller performs the augmented job transition
        Waiting>Ready>Pending>Active>Error

    """
    result = orig_write_output(job_id, content, conn=conn)
    if result:
        update_job_status(job_id, ERROR, conn=conn)
    return result

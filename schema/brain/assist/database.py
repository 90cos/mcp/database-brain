"""
the ..queries module of this library aimed
to exclusively perform atomic database actions

this module provides optional assist features

without the guarantee of atomic response from the database

This file performs non-atomic actions on the database itself

these functions

Use with care

"""


from .. import r
from ..queries.decorators import wrap_connection, WRAP_RETHINK_ERRORS


@wrap_connection
def _create_database(database_name, conn=None):
    """
    Creates a database at the connected rethink host
    Database must not already exist

    Call get_database_ref for safe provision

    :param database_name: <string>
    :param conn: <rethink.DefaultConnection>

    :return: reference to the database
    """
    r.db_create(database_name).run(conn)
    return r.db(database_name)


@wrap_connection
def _create_table(database_name, table_name, conn=None):
    """
    Creates a table at the connected rethink host within the named database
    Database must already exist
    Table must not already exist

    Call get_table_ref for safe provision

    :param database_name: <string>
    :param table_name:  <string>
    :param conn: <rethink.DefaultConnection>

    :return: reference to the table
    """
    db_ref = get_database_ref(database_name, conn=conn)
    db_ref.table_create(table_name).run(conn)
    return r.db(database_name).table(table_name)


@wrap_connection
def get_database_ref(database_name, conn=None):
    """
    Non-atomic potentially dangerous way to "safely" provision a database

    If the database already exists, this gets it.
    If the database does not exist, it creates it, then gets it

    Obvious race condition is obvious, if timing is critical
            implement safely within ones own thread

    :param database_name: <string>
    :param conn: <rethink.DefaultConnection>

    :return: Reference to the database
    """
    if database_name in r.db_list().run(conn):
        handle = r.db(database_name)
    else:
        handle = _create_database(database_name, conn)
    return handle


@wrap_connection
def get_table_ref(database_name, table_name, conn=None):
    """
    Non-atomic potentially dangerous way to "safely" provision
        a table within a database

    If the table in the database already exists, this gets it.

    If the database exists, but not the table,
        it'll create the table, then get it

    If the database does not exist,
        it creates it, then creates the table, then gets it

    Obvious race condition is obvious, if timing is critical
            implement safely within ones own thread

    :param database_name: <string>
    :param table_name: <string>
    :param conn: <rethink.DefaultConnection>

    :return: <reference to the table>
    """
    db_ref = get_database_ref(database_name, conn=conn)
    if table_name in db_ref.table_list().run(conn):
        handle = db_ref.table(table_name)
    else:
        handle = _create_table(database_name,
                               table_name).run(conn)
    return handle


@wrap_connection
def nuke_table(database_name, table_name, conn=None):
    """
    WARNING: It would be quite rude to nuke a "core" table
                We're all friends, so not bothering to check your work
                but please don't, please

    if the table didn't exist, fantastic, job is done boss
    If the table did exist, great, job is done boss

    just call this function and it'll return a boolean telling you that
        the database is gone

    :param database_name: <string>
    :param table_name: <string>
    :param conn: <rethink.DefaultConnection>
    :return: <bool> True if the table never existed or it's removed now
    """
    try:
        r.db(database_name).table_drop(table_name).run(conn)
    except WRAP_RETHINK_ERRORS:
        if database_name in r.db_list().run(conn) and \
                table_name in r.db(database_name).table_list():
            raise ValueError("Unable to nuke table - {}.{}".format(database_name,
                                                                   table_name))
    return database_name not in r.db_list().run(conn) \
        or table_name not in r.db(database_name).table_list().run(conn)


@wrap_connection
def nuke_database(database_name, conn=None):
    """
    WARNING: It would be quite rude to nuke a "core" database
                We're all friends, so not bothering to check your work
                but please don't, please


    if the database didn't exist, fantastic, job is done boss
    If the database did exist, great, job is done boss

    just call this function and it'll return a boolean telling you that
        the database is gone


    :param database_name: <string>
    :param conn: <rethink.DefaultConnection>
    :return: <bool> True if the database never existed or it's removed now
    """
    try:
        r.db_drop(database_name).run(conn)
    except WRAP_RETHINK_ERRORS:
        if database_name in r.db_list().run(conn):
            raise ValueError("Unable to nuke database - {}".format(database_name))
    return database_name not in r.db_list().run(conn)

"""
quick helper functions
"""
from . import ADDRESS_KEY, TCP_KEY, UDP_KEY, PORT_SEPARATOR, \
    MOCK_ERROR_DICT, CONFLICT_ERROR_STRING, FIRST_ERROR, \
    EX_PORTS_KEY, TCP_STR, UDP_STR


def _check_common(field, interface, port_data):
    """

    :param field:
    :param interface:
    :param port_data:
    :return:
    """
    response = {}
    common = list(set(port_data[field]) &
                  set(interface[field]))
    if common:
        msg = CONFLICT_ERROR_STRING.format(field,
                                  common,
                                  interface[ADDRESS_KEY])
        response = MOCK_ERROR_DICT
        response[FIRST_ERROR] = msg
    return response


def has_port_conflict(port_data,
                      existing):
    """

    :param port_data:
    :param existing:
    :return:
    """
    for interface in existing:
        common_tcp = _check_common(TCP_KEY, interface, port_data)
        common_udp = _check_common(UDP_KEY, interface, port_data)
        if common_tcp:
            return common_tcp
        elif common_udp:
            return common_udp
    return None


def plugin_has_interface_conflict(new_plugin, existing_interfaces):
    """
    given a new plugin object, see if its external ports are open
    by checking the interface list


    :param new_plugin:
    :param existing_interfaces:
    :return:
    """

    port_data = {ADDRESS_KEY: new_plugin[ADDRESS_KEY],
                 TCP_KEY: [],
                 UDP_KEY: []}
    for port_proto in new_plugin[EX_PORTS_KEY]:
        port, protocol = port_proto.split(PORT_SEPARATOR)
        if protocol == TCP_STR:
            port_data[TCP_KEY].append(port)
        elif protocol == UDP_STR:
            port_data[UDP_KEY].append(port)

    return has_port_conflict(port_data, existing_interfaces)

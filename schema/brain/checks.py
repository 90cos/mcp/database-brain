"""
functions to perform checks used by other areas of the brain

functions should all return a boolean if possible.
"""
from collections import defaultdict
from google.protobuf.message import EncodeError
import dict_to_protobuf #this lib allows extra keys
import re
from .static import TARGET_OPTIONAL_FIELD, ERROR, \
    TARGET, PLUGIN, JOBS, JOB, COMMAND, INPUT_FIELD, VALUE_FIELD, \
    OPTIONAL_FIELD, COMMAND_FIELD, VALIDATOR_FIELD
dict_to_protobuf.l.setLevel(ERROR.upper())


def verify(value, msg):
    """
    C-style validator

    Keyword arguments:
    value -- dictionary to validate (required)
    msg -- the protobuf schema to validate against (required)

    Returns:
        True: If valid input
        False: If invalid input
    """
    return bool(value) and \
           converts_to_proto(value, msg) and \
           successfuly_encodes(msg) and \
           special_typechecking(value, msg)


def special_typechecking(value, msg):
    """
    Special Typechecking not available via protocol itself
    :param value: <dict>
    :param msg: <proto object>
    :return: <bool>
    """
    result = CHECKING_SWITCH[msg.DESCRIPTOR.name](value)
    return result


def special_jobs_checking(jobs):
    """

    :param jobs: list of jobs
    :return: boolean representing if all jobs pass special checking
    """
    check_status = True
    if isinstance(jobs, dict):
        iterator = jobs[JOBS]
    else:
        iterator = jobs

    for job in iterator:
        if special_job_checking(job) is False:
            check_status = False

    return check_status


def special_job_checking(job):
    """

    :param job: a dictionary that adheres to Job schema
    :return: if the job's command succeeds checking
    """
    return special_command_checking(job[COMMAND_FIELD])


def special_command_checking(command):
    """

    :param command: dict that adheres to Command schema
    :return: if the command's regex match
    """
    return verify_regex(command)


def special_plugin_checking(value):
    """

    :param value:
    :return:
    """
    return verify_plugin_contents(value)


def special_target_typecheck(value):
    """
    Special type checking for the target object
    :param value: <dict>
    :return: <bool>
    """
    result = True
    # if key:Optional exists, it must be a dict object
    result &= isinstance(value.get(TARGET_OPTIONAL_FIELD, dict()), dict)
    return result


def converts_to_proto(value, msg, raise_err=False):
    """
    Boolean response if a dictionary can convert into the proto's schema

    :param value: <dict>
    :param msg: <proto object>
    :param raise_err: <bool> (default false) raise for troubleshooting
    :return: <bool> whether the dict can covert
    """
    result = True
    try:
        dict_to_protobuf.dict_to_protobuf(value, msg)
    except TypeError as type_error:
        if raise_err:
            raise type_error
        result = False
    return result


def successfuly_encodes(msg, raise_err=False):
    """
    boolean response if a message contains correct information to serialize

    :param msg: <proto object>
    :param raise_err: <bool>
    :return: <bool>
    """
    result = True
    try:
        msg.SerializeToString()
    except EncodeError as encode_error:
        if raise_err:
            raise encode_error
        result = False
    return result


def verify_regex(value):
    """
    checks both the Inputs fields and Optional fields
    passes the list down to the check regex function

    :param <dict> ( Command schema ):
    :return: <bool>
    """
    return check_regex(value[INPUT_FIELD]) and check_regex(value[OPTIONAL_FIELD])


def check_regex(inputs):
    """
    per the schema, there is an optional Validator field

    Bad (non-compileable) validators are treated as no validator.


    :param inputs: <list> from a command dict meeting the Command.Input spec
    :return: <bool
    """
    for j_input in inputs:
        try:
            reg = re.compile(j_input[VALIDATOR_FIELD])
        except (KeyError,    # plugin did not specify a validator field.
                re.error):   # plugin supplied bad validator
            continue   # no need to validate this line, no validator

        if reg.match(j_input[VALUE_FIELD]) is None:
            return False

    return True


def strip(value, msg):
    """
    Strips all non-essential keys from the value dictionary
    given the message format protobuf

    raises ValueError exception if value does not have all required keys

    :param value: <dict> with arbitrary keys
    :param msg: <protobuf> with a defined schema
    :return: NEW <dict> with keys defined by msg, omits any other key
    """
    dict_to_protobuf.dict_to_protobuf(value, msg)
    try:
        msg.SerializeToString()  #raise error for insufficient input
    except EncodeError as encode_error:
        raise ValueError(str(encode_error))
    output = dict_to_protobuf.protobuf_to_dict(msg)
    return output


def _default_plugin_validates(*args):
    """
    Validator myst return True unless there is a specific reason
    not to validate

    :param args:
    :return: True (always)
    """
    return True


def _default_checking_handler():
    """
    default switch pointer to the always validator function
    :return:
    """
    return _default_plugin_validates


CHECKING_SWITCH = defaultdict(_default_checking_handler)
CHECKING_SWITCH[JOBS] = special_jobs_checking
CHECKING_SWITCH[JOB] = special_job_checking
CHECKING_SWITCH[COMMAND] = special_command_checking
CHECKING_SWITCH[PLUGIN] = special_plugin_checking
CHECKING_SWITCH[TARGET] = special_target_typecheck

from .controller.plugins import verify_plugin_contents

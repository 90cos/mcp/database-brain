"""
functions related to moving binary objects in/out of Brain.Files
"""

from time import time, sleep
from .. import r
from ..queries.decorators import wrap_connection, wrap_rethink_errors
from ..queries.decorators import wrap_connection_reconnect_test
from ..checks import verify
from ..brain_pb2 import Binary
from ..queries import RBF, RBM
from ..queries import BRAIN_DB, FILEMETADATA
from .decorators import wrap_name_to_id, wrap_guess_content_type
from .decorators import wrap_content_as_binary_if_needed
from .decorators import wrap_split_big_content
from .decorators import wrap_insure_files_meta_table
from . import PRIMARY_FIELD, CONTENT_FIELD, TIMESTAMP_FIELD
from . import PART_FIELD, PARTS_FIELD, PARENT_FIELD
from . import FNFERR


BINARY = r.binary


def _write_concern(file_name, file_name_check, conn, **kwargs):
    """
    Checks to see if Brain.Filemetadata was
    updated using delete or put
    :param file_name:
    :param file_name_check:
    :param conn:
    :param kwargs: wc=write concern (0,1), ws_timeout=timeout int
    :return:
    """
    file_name_bool = file_name_check

    if kwargs.get("wc", 1):
        wait_time = kwargs.get("wc_timeout", 40) + time()
        while time() < wait_time:
            file_name_bool = file_name in list_dir_with_parts(conn=conn)
            if file_name_check != file_name_bool:
                break
            sleep(0.2)
        if file_name_check == file_name_bool:
            raise IOError("Brain.Filemetadata wasn't updated in time")


@wrap_rethink_errors
@wrap_connection_reconnect_test
@wrap_connection
@wrap_name_to_id
@wrap_guess_content_type
@wrap_split_big_content
@wrap_content_as_binary_if_needed
@wrap_insure_files_meta_table
def put(obj_dict, conn=None, **kwargs):
    """
    This function might throw an error like:
        Query size (290114573) greater than maximum (134217727)
    caller may need to manually split files at this time
    :param obj_dict: <dict> matching brain_pb2.Binary object
    :param conn:
    :param kwargs:
    :return:
    """
    if kwargs.get("verify", False):
        verify(obj_dict, Binary())
    inserted = RBF.insert(obj_dict).run(conn)
    _write_concern(obj_dict[PRIMARY_FIELD], False, conn, **kwargs)
    return inserted


def put_buffer(filename, content, conn=None, **kwargs):
    """
    helper function for put
    :param filename: <str>
    :param content: <bytes>
    :param conn: <rethinkdb.DefaultConnection>
    :return: <dict>
    """
    obj_dict = {PRIMARY_FIELD: filename,
                CONTENT_FIELD: content,
                TIMESTAMP_FIELD: time()}
    return put(obj_dict, conn=conn, **kwargs)


@wrap_rethink_errors
@wrap_connection
def get(filename, conn=None):
    """

    :param filename:
    :param conn:
    :return: <dict>
    """
    res_file = RBF.get(filename).run(conn)
    if not res_file:
        err = FNFERR.format(filename,
                            conn.host,
                            conn.port)
        raise FileNotFoundError(err)
    full_content = b""
    try:
        for part in res_file[PARTS_FIELD]:
            part_file = RBF.get(part).run(conn)
            full_content += part_file[CONTENT_FIELD]

        res_file[CONTENT_FIELD] = full_content
    except KeyError:
        return res_file
    return res_file


@wrap_rethink_errors
@wrap_connection
def resync_file_meta_data(conn=None):
    insert_data = []
    RBM.delete().run(conn)
    dir_list = list_dir(force=True, conn=conn)

    for file_name in dir_list:
        insert_data.append({PRIMARY_FIELD: file_name})
    results = RBM.insert(insert_data).run(conn)
    return results


@wrap_rethink_errors
@wrap_connection
def list_dir(force=False, conn=None):
    """

    :param conn:
    :return: <list>
    """
    # Checking if Brain.Filesmetadata table exist
    if r.db(BRAIN_DB).table_list().contains(FILEMETADATA).run(conn) and not force:
        available = RBM.filter({PART_FIELD: False}).order_by(PRIMARY_FIELD).run(conn)
    else:
        available = RBF.filter({PART_FIELD: False}).\
            pluck(PRIMARY_FIELD).\
            order_by(PRIMARY_FIELD).\
            run(conn)
    return [x[PRIMARY_FIELD] for x in available]


@wrap_rethink_errors
@wrap_connection
@wrap_insure_files_meta_table
def list_dir_with_parts(conn=None):
    """

    :param conn:
    :return: <list>
    """
    available = RBM.order_by(PRIMARY_FIELD).run(conn)
    return [x[PRIMARY_FIELD] for x in available]


@wrap_rethink_errors
@wrap_connection
def delete(filename, conn=None, **kwargs):
    """
    deletes a file
    filename being a value in the "id" key
    :param filename: <str>
    :param conn: <rethinkdb.DefaultConnection>
    :return: <dict>
    """
    deleted = RBF.filter((r.row[PRIMARY_FIELD] == filename) | (r.row[PARENT_FIELD] == filename)).delete().run(conn)
    _write_concern(filename, True, conn, **kwargs)
    return deleted

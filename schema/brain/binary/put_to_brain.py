"""
puts a file into the brain
"""

if __name__.endswith("__main__"):
    from brain import connect
    from brain.binary import put_buffer
    from sys import argv, stderr, exit

    if len(argv) < 3:
        stderr.write("usage:\n{} <brain-ip-or-hostname> <filename-to-upload-to-brain>\n".format(argv[0]))
        exit(-1)
    else:
        conn = connect(host=argv[1])
        brain_file_name = argv[2][argv[2].rindex("/")+1:]
        with open(argv[2], "rb") as f:
            data = f.read()
            put_buffer(brain_file_name, data, conn=conn)
            print("put {} in brain ({})".format(brain_file_name,
                                                argv[1]))

"""
Pytest file for the queries
"""

from os import environ
from pytest import fixture, raises
from time import sleep
import docker
from types import GeneratorType
from .brain import connect, r
from .brain.jobs import InvalidStateTransition, InvalidState
from .brain import queries
from .brain import assist
from .brain.static import RBJ, RBO, PENDING, DONE, WAITING, READY, ERROR
from copy import deepcopy
CLIENT = docker.from_env()


@fixture(scope='module')
def rethink():
    sleep(10)  # prior test docker needs to shut down
    tag = "dev"
    container_name = "brainmodule_assist_db_table_test"
    container = CLIENT.containers.run(
        "registry.gitlab.com/90cos/mcp/database-brain:{}".format(tag),
        name=container_name,
        detach=True,
        ports={"28015/tcp": 28015},
        remove=True
    )
    sleep(10)
    yield True
    # Teardown for module tests
    container.stop()


def test_starting_fresh(rethink):
    conn = connect()
    assert "db" not in r.db_list().run(conn)


def test_make_the_database_by_refering_to_it(rethink):
    conn = connect()
    assist.database.get_database_ref("db")
    sleep(2)  #
    assert True  # woo no exception!
    assert "db" in r.db_list().run(conn)


def test_make_table_in_known_db_by_refering_to_it(rethink):
    conn = connect()
    assert "db" in r.db_list().run(conn)
    assist.database.get_table_ref("db", "table")
    assert True  # woo no exception!
    assert "table" in r.db("db").table_list().run(conn)


def test_soft_removal_of_the_table(rethink):
    conn = connect()
    assert "table" in r.db("db").table_list().run(conn)
    donezie = assist.database.nuke_table("db", "table")
    assert donezie  # because it is true if it worked
    assert "db" in r.db_list().run(conn)
    assert "table" not in r.db("db").table_list().run(conn)


def test_soft_removal_of_the_database(rethink):
    conn = connect()
    assert "db" in r.db_list().run(conn)
    donezie = assist.database.nuke_database("db")
    assert donezie  # because it is true if it worked
    assert "db" not in r.db_list().run(conn)


def test_soft_create_table_in_db_that_does_not_exist(rethink):
    conn = connect()
    assert "hahaNotHere" not in r.db_list().run(conn)
    ref = assist.database.get_table_ref("hahaNotHere", "butIsNow")
    assert ref
    assert "hahaNotHere" in r.db_list().run(conn)
    assert "butIsNow" in r.db("hahaNotHere").table_list().run(conn)


def test_soft_removal_of_the_database_with_existing_tables(rethink):
    conn = connect()
    assert "hahaNotHere" in r.db_list().run(conn)
    assert "butIsNow" in r.db("hahaNotHere").table_list().run(conn)
    donezie = assist.database.nuke_database("hahaNotHere")
    assert donezie  # because it is true if it worked
    assert "hahaNotHere" not in r.db_list().run(conn)


def test_soft_removal_of_the_database_that_doesnt_even_exist(rethink):
    conn = connect()
    assert "whatAreYouEvenDoingMan" not in r.db_list().run(conn)
    donezie = assist.database.nuke_database("whatAreYouEvenDoingMan")
    assert donezie  # because it is true if it worked
    assert "whatAreYouEvenDoingMan" not in r.db_list().run(conn)


def test_soft_removal_of_table_in_the_database_that_doesnt_even_exist(rethink):
    conn = connect()
    assert "whatAreYouEvenDoingManAGAIN" not in r.db_list().run(conn)
    donezie = assist.database.nuke_table("whatAreYouEvenDoingManAGAIN", "whatAreYouEvenDoingManAGAINsUncle")
    assert donezie  # because it is true if it worked
    assert "whatAreYouEvenDoingManAGAIN" not in r.db_list().run(conn)

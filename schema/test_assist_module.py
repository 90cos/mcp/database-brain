"""
Pytest file for the queries
"""

from os import environ
from pytest import fixture, raises
from time import sleep
import docker
from types import GeneratorType
from .brain import connect, r
from .brain.jobs import InvalidStateTransition, InvalidState
from .brain import queries
from .brain import assist
from .brain.static import RBJ, RBO, PENDING, DONE, WAITING, READY, ERROR
from copy import deepcopy
CLIENT = docker.from_env()


TEST_TARGET = {"PluginName": "TestPlugin",
               "Location": "0.0.0.0",
               "Port": "0",
               "Optional": "example"}
TEST_CAPABILITY = [
    {
        "CommandName": "echo",
        "Tooltip": "",
        "Output": True,
        "Inputs": [
                {"Name": "EchoString",
                 "Type": "textbox",
                 "Tooltip": "This string will be echoed back",
                 "Value": ""
                 },
                ],
        "OptionalInputs": []
    },
    {
        "CommandName": "aardvark",
        "Tooltip": "should be first!",
        "Output": False,
        "Inputs": [],
        "OptionalInputs": []
    },
    {
        "CommandName": "zoom",
        "Tooltip": "should be last!",
        "Output": False,
        "Inputs": [],
        "OptionalInputs": []
    },
]

TEST_JOB = {
    "JobTarget": TEST_TARGET,
    "Status": "Ready",
    "StartTime": 7,
    "JobCommand": TEST_CAPABILITY[0]
}


@fixture(scope='module')
def rethink():
    sleep(3)  # prior test docker needs to shut down
    tag = "dev"
    container_name = "brainmodule_assist_test"
    container = CLIENT.containers.run(
        "registry.gitlab.com/90cos/mcp/database-brain:{}".format(tag),
        name=container_name,
        detach=True,
        ports={"28015/tcp": 28015},
        remove=True
    )
    sleep(2)
    yield True
    # Teardown for module tests
    container.stop()


@fixture(scope="function")
def job_cleanup():
    return RBJ.delete().run(connect())


@fixture(scope="function")
def output_cleanup():
    return RBO.delete().run(connect())


def test_add_target(rethink):
    inserted = queries.insert_target(TEST_TARGET, False, connect())
    assert isinstance(inserted, dict)
    assert isinstance(inserted['generated_keys'], list)
    assert len( inserted['generated_keys'] ) == 1


def test_get_targets_one(rethink):
    g = queries.get_targets()
    assert isinstance(g, GeneratorType)
    target = g.__next__()
    assert isinstance(target, dict)
    with raises(StopIteration):
        g.__next__()


def test_helper_job_good_status_update(rethink, job_cleanup):
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                 TEST_TARGET["Location"],
                                 TEST_TARGET['Port'],
                                 conn=connect(),
                                 status=PENDING)
    assert g["Status"] == PENDING


def test_helper_job_invalid_transition_status_update(rethink, job_cleanup):
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    with raises(InvalidStateTransition):
        g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                     TEST_TARGET["Location"],
                                     TEST_TARGET['Port'],
                                     conn=connect(),
                                     status=WAITING)


def test_helper_job_invalid_status_before_update(rethink, job_cleanup):
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    with raises(InvalidStateTransition):
        g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                     TEST_TARGET["Location"],
                                     TEST_TARGET['Port'],
                                     conn=connect(),
                                     status="NOT-EVEN-A-REAL-STATE")


def test_helper_job_why_did_you_even_call_this_then(rethink, job_cleanup):
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                 TEST_TARGET["Location"],
                                 TEST_TARGET['Port'],
                                 conn=connect())
    assert g["Status"] == READY


def test_helper_job_output_is_yay(rethink, job_cleanup, output_cleanup):
    conn = connect()
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                 TEST_TARGET["Location"],
                                 TEST_TARGET['Port'],
                                 conn=conn,
                                 status=PENDING)
    assist.jobs.write_output(g['id'], "yay", conn=conn)
    job = RBJ.get(g['id']).run(conn)
    assert job["Status"] == DONE
    outputs = RBO.filter({
        "OutputJob": {
            "id": g['id']
        }
    }).run(conn)
    found = False
    for output in outputs:
        found = True
        assert output['Content'] == "yay"
        assert output['OutputJob']['Status'] == DONE
    assert found, "Could not find output in the RBO table"


def test_helper_job_error_is_yay(rethink, job_cleanup, output_cleanup):
    conn = connect()
    res = queries.insert_jobs([TEST_JOB])
    assert isinstance(res, dict)
    assert isinstance(res['generated_keys'], list)
    assert len(res['generated_keys']) == 1
    g = assist.jobs.get_next_job(TEST_TARGET['PluginName'],
                                 TEST_TARGET["Location"],
                                 TEST_TARGET['Port'],
                                 conn=conn,
                                 status=PENDING)
    assist.jobs.write_error(g['id'], "yay", conn=conn)
    job = RBJ.get(g['id']).run(conn)
    assert job["Status"] == ERROR
    outputs = RBO.filter({
        "OutputJob": {
            "id": g['id']
        }
    }).run(conn)
    found = False
    for output in outputs:
        found = True
        assert output['Content'] == "yay"
        assert output['OutputJob']['Status'] == ERROR
    assert found, "Could not find output in the RBO table"

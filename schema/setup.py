#!/usr/bin/python
"""Docstrings"""
from setuptools import setup, find_packages

VERSION = "0.1.89"

setup(
    name="ramrodbrain",
    version=VERSION,
    packages=find_packages(),
    description='RethinkDB Wrapper functions',
    author='Dan Bauman',
    maintainer='Dan Bauman',
    maintainer_email='dan@bauman.space',
    license='MIT',
    url='https://gitlab.com/90cos/mcp/database-brain',
    download_url='https://gitlab.com/90cos/mcp/database-brain',
    install_requires=[
        'rethinkdb==2.3.0.post6',
        "protobuf==3.20.3",
        "dict-to-protobuf",
        "decorator==4.4.2",  # v5 eliminated replacing absent args with None
        "python-magic"
    ],
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
)

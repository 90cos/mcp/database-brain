from .brain import checks
from .brain.brain_pb2 import Command, Job, Jobs

COMM_REGEX_VALID = {"CommandName": "anystring",
              "Tooltip": "otherstring",
              "Output": True,
              "Inputs": [{"Name" : "string",
                          "Type": "String",
                          "Tooltip": "String",
                          "Value": "Testing",
                          "Validator": "Testing"}],
              "OptionalInputs": [{"Name" : "string",
                                  "Type": "String",
                                  "Tooltip": "String",
                                  "Value": "Hello",
                                  "Validator": "Hello"}]}

COMM_REGEX_FAIL = {"CommandName": "anystring",
              "Tooltip": "otherstring",
              "Output": True,
              "Inputs": [{"Name" : "string",
                          "Type": "String",
                          "Tooltip": "String",
                          "Value": "testing",
                          "Validator": "Testing"}],
              "OptionalInputs": [{"Name" : "string",
                                  "Type": "String",
                                  "Tooltip": "String",
                                  "Value": "Hello",
                                  "Validator": "Hello"}]}

COMM_REGEX_OP_FAIL = {"CommandName": "anystring",
              "Tooltip": "otherstring",
              "Output": True,
              "Inputs": [{"Name" : "string",
                          "Type": "String",
                          "Tooltip": "String",
                          "Value": "Testing",
                          "Validator": "Testing"}],
              "OptionalInputs": [{"Name" : "string",
                                  "Type": "String",
                                  "Tooltip": "String",
                                  "Value": "hello",
                                  "Validator": "Hello"}]}

COMM_REGEX_EMPTY = {"CommandName": "anystring",
              "Tooltip": "otherstring",
              "Output": True,
              "Inputs": [{"Name" : "string",
                          "Type": "String",
                          "Tooltip": "String",
                          "Value": "",
                          "Validator": "Testing"}],
              "OptionalInputs": [{"Name" : "string",
                                  "Type": "String",
                                  "Tooltip": "String",
                                  "Value": "",
                                  "Validator": "Hello"}]}

COMM_REGEX_STUFF = {
  'CommandName': 'List Files/Directories',
  'Inputs': [{'Name': 'Full Path To File or Directory',
              'Tooltip': 'Full path to directory.',
              'Type': 'textbox',
              'Validator': '^[a-zA-Z]:[\\\\/].{0,260}$',
              'ValidatorMessage': 'Must be a valid file path value',
              'Value': 'c:\\users\\test\\Documents\\anything.txt'},
             {'Name': 'Wildcard (*)',
              'Tooltip': 'Wilcard delimiter must be *',
              'Type': 'textbox',
              'Validator': '^\\*$',
              'ValidatorMessage': 'Must be *',
              'Value': '*'},
             {'Name': 'Directory Depth',
              'Tooltip': 'Directory depth',
              'Type': 'textbox',
              'Validator': '^\\d+$',
              'ValidatorMessage': 'Must be a positive number',
              'Value': '200'},
             {'Name': 'Max Return Size (999999)',
              'Tooltip': 'Maximum return size must be 999999',
              'Type': 'textbox',
              'Validator': '^999999$',
              'ValidatorMessage': 'Must be 999999',
              'Value': '999999'}],
  'OptionalInputs': [],
  'Output': True,
  'Tooltip': ''}

COMM_NO_REGEX = {"CommandName": "anystring",
              "Tooltip": "otherstring",
              "Output": True,
              "Inputs": [{"Name" : "string",
                          "Type": "String",
                          "Tooltip": "String",
                          "Value": ""}],
              "OptionalInputs": []}

Good_TARGET = {
    "PluginName": "WaterBalloon",
    "Location": "Patio",
    "Port": "West",
    "Optional": {"anything": "anything"},
}

JOB_GOOD_REGEX = {"id": "string",
           "JobTarget": Good_TARGET,
           "Status": "string",
           "StartTime": 0,
           "JobCommand": COMM_REGEX_VALID,}

JOB_BAD_REGEX = {"id": "string",
           "JobTarget": Good_TARGET,
           "Status": "string",
           "StartTime": 0,
           "JobCommand": COMM_REGEX_FAIL,}

JOB_NO_REGEX = {"id": "string",
           "JobTarget": Good_TARGET,
           "Status": "string",
           "StartTime": 0,
           "JobCommand": COMM_NO_REGEX,}


def test_regex_validation():
    assert checks.special_typechecking(COMM_REGEX_VALID, Command())


def test_regex_fail():
    assert checks.special_typechecking(COMM_REGEX_FAIL, Command()) is False


def test_regex_op_fail():
    assert checks.special_typechecking(COMM_REGEX_OP_FAIL, Command()) is False


def test_regex_empty():
    assert checks.special_typechecking(COMM_REGEX_EMPTY, Command()) is False


def test_no_regex():
    assert checks.special_typechecking(COMM_NO_REGEX, Command())


def test_real_regex():
    assert checks.special_typechecking(COMM_REGEX_STUFF, Command())


def test_job_regex():
    assert checks.special_typechecking(JOB_GOOD_REGEX, Job())


def test_job_no_regex():
    assert checks.special_typechecking(JOB_NO_REGEX, Job())


def test_job_regex_fail():
    assert checks.special_typechecking(JOB_BAD_REGEX, Job()) is False


def test_jobs_regex():
    jobs = [JOB_GOOD_REGEX, JOB_GOOD_REGEX]
    assert checks.special_typechecking(jobs, Jobs())


def test_jobs_regex_fail():
    jobs = [JOB_GOOD_REGEX, JOB_BAD_REGEX]
    assert checks.special_typechecking(jobs, Jobs()) is False


def test_jobs_no_regex():
    jobs = [JOB_NO_REGEX, JOB_NO_REGEX]
    assert checks.special_typechecking(jobs, Jobs())
